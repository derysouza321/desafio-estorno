FROM amazoncorretto:21 AS builder

RUN yum -y install maven

WORKDIR /app

COPY pom.xml .
COPY src ./src

RUN mvn clean package

RUN rm -f /docker-entrypoint-initdb.d/*

ENV MYSQL_ROOT_PASSWORD=kbm123
ENV MYSQL_DATABASE=luiza_labs
ENV MYSQL_USER=luiza_labs
ENV MYSQL_PASSWORD=kbm123

COPY src/main/resources/database/V1_USUARIOS.sql /docker-entrypoint-initdb.d/
COPY src/main/resources/database/V2_PEDIDOS.sql /docker-entrypoint-initdb.d/
COPY src/main/resources/database/V3_PRODUTOS.sql /docker-entrypoint-initdb.d/
COPY src/main/resources/database/V4_PEDIDOS_PRODUTOS.sql /docker-entrypoint-initdb.d/
COPY src/main/resources/database/V5_ESTORNOS.sql /docker-entrypoint-initdb.d/
