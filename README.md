# LuizaLabs - Desafio técnico - Estorno

Este projeto foi desenvolvido com o objetivo de simular uma API de integração que recebe arquivos desnormalizados de um sistema legado. 
A função principal é processar esses dados e inseri-los em um banco MySQL. 
O objetivo é garantir que esses dados sejam formatados e salvos de forma consistente, evitando qualquer perda de informação durante o processo.

## Tecnologias

* Java 21
* Maven  
* Mysql
* Docker 

## Utilização
Este projeto é dockerizado e utiliza um banco de dados MySQL. Siga os passos abaixo para configurá-lo e executá-lo usando Docker Compose:

Passo 1: Buildar e Rodar os Contêineres com Docker Compose
docker-compose up --build -d

Passo 2: Configurar a Conexão com o Banco de Dados
Se ao conectar ao banco de dados com o DBeaver você encontrar o erro "Public Key Retrieval is not allowed", vá em Driver Properties e adicione:
allowPublicKeyRetrieval=true
useSSL=false

Passo 3: Executar a Aplicação
Certifique-se de que o banco de dados está ativo.
Execute a classe EstornosApplication na sua IDE para processar os arquivos e inserir os dados formatados no banco.

## Pipelines
Este projeto possui pipelines de CI/CD que realizará:

*Build*: Compila o projeto Maven e gera o pacote do aplicativo.
*Testes Unitários*: Executa os testes unitários do projeto.
*Análise com SonarQube*: Realiza a análise estática do código-fonte com o SonarQube para verificar a qualidade e possíveis problemas no código.


