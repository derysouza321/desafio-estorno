package org.luizalabs.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class PedidoDto {
    private int pedidoId;
    private BigDecimal total;
    private LocalDate data;
    private List<ProdutoDto> produtos;

    public int getPedidoId() {
        return pedidoId;
    }

    public void setPedidoId(int pedidoId) {
        this.pedidoId = pedidoId;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public List<ProdutoDto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<ProdutoDto> produtos) {
        this.produtos = produtos;
    }

    @Override
    public String toString() {
        return String.format("\t\t{\n\t\t\t\"order_id\": %d,\n\t\t\t\"total\": \"%s\",\n\t\t\t\"date\": \"%s\",\n\t\t\t\"products\": [\n%s\t\t\t]\n\t\t}",
                pedidoId, total, data, produtos.stream()
                        .map(ProdutoDto::toString)
                        .collect(Collectors.joining(",\n")));
    }
}
