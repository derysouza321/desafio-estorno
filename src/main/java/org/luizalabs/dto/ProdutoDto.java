package org.luizalabs.dto;

import java.math.BigDecimal;

public class ProdutoDto {
    private int produtoId;
    private BigDecimal valor;

    public int getProdutoId() {
        return produtoId;
    }

    public void setProdutoId(int produtoId) {
        this.produtoId = produtoId;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        return String.format("\t\t\t\t{\n\t\t\t\t\t\"product_id\": %d,\n\t\t\t\t\t\"value\": \"%s\"\n\t\t\t\t}",
                produtoId, valor);
    }
}
