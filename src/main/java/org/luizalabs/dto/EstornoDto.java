package org.luizalabs.dto;

import java.util.stream.Collectors;

public class EstornoDto {

    private UsuarioDto usuario;

    public UsuarioDto getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDto usuario) {
        this.usuario = usuario;
    }

    @Override
    public String toString() {
        String usuarioJson = String.format("\n\t\"user_id\": %d,\n\t\"name\": \"%s\"",
                usuario.getUsuarioId(), usuario.getName());

        String pedidosJson = usuario.getPedidos().stream()
                .map(PedidoDto::toString)
                .collect(Collectors.joining(",\n"));

        return String.format("{\n\"usuario\": %s,\n\"pedidos\": [\n%s\n]\n}",
                usuarioJson, pedidosJson);
    }
}
