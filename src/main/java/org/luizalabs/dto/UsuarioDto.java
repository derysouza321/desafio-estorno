package org.luizalabs.dto;


import java.util.List;
import java.util.stream.Collectors;

public class UsuarioDto {
    private int usuarioId;
    private String name;
    private List<PedidoDto> pedidos;

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PedidoDto> getPedidos() {
        return pedidos;
    }

    public void setPedidos(List<PedidoDto> pedidos) {
        this.pedidos = pedidos;
    }

    @Override
    public String toString() {
        String pedidosJson = pedidos.stream()
                .map(PedidoDto::toString)
                .collect(Collectors.joining(",\n"));

        return String.format("{\n\"usuarioId\": %d,\n\"name\": \"%s\",\n\"orders\": [\n%s\n]\n}",
                usuarioId, name, pedidosJson);
    }
}
