package org.luizalabs.service;

import org.luizalabs.dto.ArquivoDto;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.luizalabs.service.util.Util.SCRIPT_FILE;
import static org.luizalabs.service.util.Util.parseDate;

public class ArquivoService {
    private static final Logger logger = Logger.getLogger(ArquivoService.class.getName());
    private final Set<Integer> usuariosInseridos = new HashSet<>();
    private final Set<Integer> pedidosInseridos = new HashSet<>();
    private final Set<Integer> produtosInseridos = new HashSet<>();

    private final List<String> scriptsInsert = new ArrayList<>();

    public void processarArquivos(String usuariosFilePath1, String usuariosFilePath2) {
        try {
            if (!scriptJaProcessado(SCRIPT_FILE)) {
                gerarScriptsInsert(usuariosFilePath1);
                gerarScriptsInsert(usuariosFilePath2);
                organizarScriptsInsert();
                escreverArquivoScriptsInsert();
                logger.info("Novo arquivo de script INSERT gerado com sucesso: " + scriptJaProcessado(SCRIPT_FILE));
            }
        } catch (IOException | ParseException e) {
            logger.log(Level.SEVERE, "Erro ao processar arquivos de usuários ou gerar script INSERT: " + e.getMessage(), e);
        }
    }

    public boolean scriptJaProcessado(String filePath) throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(filePath));
        for (String line : lines) {
            if (line.contains("INSERT")) {
                return true;
            }
        }
        return false;
    }
    private void gerarScriptsInsert(String filePath) throws IOException, ParseException {
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            Map<Integer, List<ArquivoDto>> pedidosMap = new HashMap<>();

            while ((line = reader.readLine()) != null) {
                ArquivoDto pedidoDTO = pegarDadosArquivo(line);
                pedidosMap.computeIfAbsent(pedidoDTO.getOrderId(), k -> new ArrayList<>()).add(pedidoDTO);
            }

            processarPedidosAgrupados(pedidosMap);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Erro ao ler o arquivo de usuários: " + e.getMessage(), e);
            throw e;
        }
    }

    private void processarPedidosAgrupados(Map<Integer, List<ArquivoDto>> dadosArquivo) {
        for (List<ArquivoDto> arquivoDtos : dadosArquivo.values()) {
            ArquivoDto dadosPedidoProdutos = arquivoDtos.get(0);
            BigDecimal totalValue = arquivoDtos.stream()
                    .map(ArquivoDto::getValue)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);

            dadosPedidoProdutos.setValue(totalValue);
            processarPedidoVerificarDuplicidade(dadosPedidoProdutos, arquivoDtos);
        }
    }

    private void processarPedidoVerificarDuplicidade(ArquivoDto dadosArquivo, List<ArquivoDto> produtosPedido) {
        int userId = dadosArquivo.getUserId();
        int orderId = dadosArquivo.getOrderId();

        if (!usuariosInseridos.contains(userId)) {
            usuariosInseridos.add(userId);
            gerarScriptInsertUsuario(userId, dadosArquivo.getUserName());
        }

        if (!pedidosInseridos.contains(orderId)) {
            pedidosInseridos.add(orderId);
            gerarScriptInsertPedido(orderId, userId, dadosArquivo.getValue(), dadosArquivo.getDate());
            gerarScriptInsertEstorno(userId);
        }

        verificarProdutos(produtosPedido, orderId);
    }

    private void verificarProdutos(List<ArquivoDto> produtosPedido, int orderId) {
        Set<Integer> produtosJaInseridos = new HashSet<>();
        for (ArquivoDto dados : produtosPedido) {
            int prodId = dados.getProdId();

            if (!produtosInseridos.contains(prodId)) {
                produtosInseridos.add(prodId);
                gerarScriptInsertProduto(prodId, dados.getValue());
            }

            if (!produtosJaInseridos.contains(prodId)) {
                produtosJaInseridos.add(prodId);
                gerarScriptInsertPedidoProduto(orderId, prodId);
            }
        }
    }

    private ArquivoDto pegarDadosArquivo(String line) throws ParseException {
        String userIdStr = line.substring(0, 10).trim();
        String userName = line.substring(10, 55).trim();
        String orderIdStr = line.substring(55, 65).trim();
        String prodIdStr = line.substring(65, 75).trim();
        String valueStr = line.substring(75, 87).trim();
        String dateStr = line.substring(87, 95).trim();

        int userId = Integer.parseInt(userIdStr);
        int orderId = Integer.parseInt(orderIdStr);
        int prodId = Integer.parseInt(prodIdStr);
        BigDecimal value = new BigDecimal(valueStr);
        Date date = parseDate(dateStr);

        ArquivoDto dadosDto = new ArquivoDto();
        dadosDto.setUserId(userId);
        dadosDto.setUserName(userName);
        dadosDto.setOrderId(orderId);
        dadosDto.setProdId(prodId);
        dadosDto.setValue(value);
        dadosDto.setDate(date);

        return dadosDto;
    }

    private void gerarScriptInsertUsuario(int userId, String nomeUsuario) {
        nomeUsuario = nomeUsuario.trim().replace("'", "''");
        scriptsInsert.add(String.format("INSERT IGNORE INTO Usuarios (usuario_id, nome) VALUES (%d, '%s');", userId, nomeUsuario));
    }


    private void gerarScriptInsertPedido(int orderId, int userId, BigDecimal totalValue, Date dataPedido) {
        scriptsInsert.add(String.format("INSERT IGNORE INTO Pedidos (pedido_id, usuario_id, total, data) VALUES (%d, %d, %.2f, '%tF');",
                orderId, userId, totalValue, dataPedido));
    }

    private void gerarScriptInsertProduto(int prodId, BigDecimal valorProduto) {
        scriptsInsert.add(String.format("INSERT IGNORE INTO Produtos (produto_id, valor) VALUES (%d, %.2f);", prodId, valorProduto));
    }

    private void gerarScriptInsertPedidoProduto(int orderId, int prodId) {
        scriptsInsert.add(String.format("INSERT IGNORE INTO Pedidos_Produtos (pedido_id, produto_id) VALUES (%d, %d);", orderId, prodId));
    }

    private void gerarScriptInsertEstorno(int userId) {
        scriptsInsert.add(String.format("INSERT IGNORE INTO Estornos (usuario_id) VALUES (%d);", userId));
    }


    private void organizarScriptsInsert() {
        scriptsInsert.sort(Collections.reverseOrder());
    }

    private void escreverArquivoScriptsInsert() throws IOException {
        try (PrintWriter writer = new PrintWriter(new FileWriter(SCRIPT_FILE, false))) {
            for (String script : scriptsInsert) {
                writer.println(script);
            }
        }
    }
}
