package org.luizalabs.service;
import org.luizalabs.dto.EstornoDto;
import org.luizalabs.dto.PedidoDto;
import org.luizalabs.dto.ProdutoDto;
import org.luizalabs.dto.UsuarioDto;
import org.luizalabs.exception.SQLCustomException;
import org.luizalabs.model.Estorno;
import org.luizalabs.model.Pedido;
import org.luizalabs.model.Produto;
import org.luizalabs.model.Usuario;
import org.luizalabs.repository.EstornosRepository;
import java.util.List;
import java.util.stream.Collectors;

public class EstornosService {
    private final EstornosRepository estornosRepository = new EstornosRepository();

    public List<EstornoDto> buscarEstornosCompletos() throws SQLCustomException {
        List<Estorno> estornos = estornosRepository.buscarEstornosCompletos();

        return estornos.stream()
                .map(this::buildEstornoDto)
                .collect(Collectors.toList());
    }

    private EstornoDto buildEstornoDto(Estorno estorno) {
        EstornoDto estornoDto = new EstornoDto();
        estornoDto.setUsuario(buildUsuarioDto(estorno.getUsuario()));
        return estornoDto;
    }

    private UsuarioDto buildUsuarioDto(Usuario usuario) {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setUsuarioId(usuario.getUsuarioId());
        usuarioDto.setName(usuario.getNome());
        usuarioDto.setPedidos(usuario.getPedidos().stream()
                .map(this::buildPedidoDto)
                .collect(Collectors.toList()));
        return usuarioDto;
    }

    private PedidoDto buildPedidoDto(Pedido pedido) {
        PedidoDto pedidoDto = new PedidoDto();
        pedidoDto.setPedidoId(pedido.getPedidoId());
        pedidoDto.setTotal(pedido.getTotal());
        pedidoDto.setData(pedido.getData());
        pedidoDto.setProdutos(pedido.getProdutos().stream()
                .map(this::buildProdutoDto)
                .collect(Collectors.toList()));
        return pedidoDto;
    }

    private ProdutoDto buildProdutoDto(Produto produto) {
        ProdutoDto produtoDto = new ProdutoDto();
        produtoDto.setProdutoId(produto.getProdutoId());
        produtoDto.setValor(produto.getValor());
        return produtoDto;
    }

}
