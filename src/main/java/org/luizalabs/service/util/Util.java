package org.luizalabs.service.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Util {
    public static final String PATH_DATA1 = "src/main/resources/database/data_1.txt";
    public static final String PATH_DATA2 = "src/main/resources/database/data_2.txt";
    public static final String  SCRIPT_FILE = "src/main/resources/database/script_insert.sql";

    public static Date parseDate(String dateStr) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        return sdf.parse(dateStr);
    }
}
