package org.luizalabs.connectiondb;


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Objects;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.luizalabs.service.util.Util.SCRIPT_FILE;

public class DbConnectionManager {
    private static final String CONFIG_FILE = ".env";
    private static final String DB_URL;
    private static final String DB_USUARIO;
    private static final String DB_SENHA;
    private static final Logger logger = Logger.getLogger(DbConnectionManager.class.getName());
    static {
        Properties prop = new Properties();
        try (FileInputStream input = new FileInputStream(CONFIG_FILE)) {
            prop.load(input);
            DB_URL = prop.getProperty("DB_URL");
            DB_USUARIO = prop.getProperty("DB_USUARIO");
            DB_SENHA = prop.getProperty("DB_SENHA");

            validarProperties(DB_URL, "DB_URL");
            validarProperties(DB_USUARIO, "DB_USUARIO");
            validarProperties(DB_SENHA, "DB_SENHA");
        } catch (IOException ex) {
            throw new RuntimeException("Falha ao carregar o arquivo de configuração", ex);
        }
    }

    private static void validarProperties(String property, String propertyName) {
        if (Objects.isNull(property) || property.trim().isEmpty()) {
            throw new IllegalArgumentException("A propriedade " + propertyName + " não está definida ou está vazia no arquivo de configuração.");
        }
    }

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(DB_URL, DB_USUARIO, DB_SENHA);
    }

    public void executarScriptInsert() {
        try (Connection connection = getConnection();
             Statement statement = connection.createStatement()) {

            try (FileReader fileReader = new FileReader(SCRIPT_FILE);
                 BufferedReader reader = new BufferedReader(fileReader)) {
                String line;
                while ((line = reader.readLine()) != null) {
                    if (!line.trim().isEmpty()) {
                        statement.executeUpdate(line);
                        logger.info("Comando INSERT executado com sucesso: " + line);
                    }
                }
            }
        } catch (SQLException | IOException e) {
            logger.log(Level.SEVERE, "Erro ao executar script INSERT: " + e.getMessage(), e);
        }
    }
}
