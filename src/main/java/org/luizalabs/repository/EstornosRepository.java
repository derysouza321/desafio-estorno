package org.luizalabs.repository;

import org.luizalabs.connectiondb.DbConnectionManager;
import org.luizalabs.exception.SQLCustomException;
import org.luizalabs.model.Estorno;
import org.luizalabs.model.Pedido;
import org.luizalabs.model.Produto;
import org.luizalabs.model.Usuario;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class EstornosRepository {
    private final DbConnectionManager dbConnectionManager = new DbConnectionManager();
    private static final String SQL_BUSCAR_ESTORNOS_COMPLETOS =
            "SELECT e.estorno_id, u.usuario_id, u.nome AS nome_usuario, " +
                    "p.pedido_id, p.total AS total_pedido, p.data AS data_compra, " +
                    "GROUP_CONCAT(pr.produto_id ORDER BY pr.produto_id) AS produtos_id, " +
                    "GROUP_CONCAT(pr.valor ORDER BY pr.produto_id) AS valores_produtos " +
                    "FROM Estornos e " +
                    "JOIN Usuarios u ON e.usuario_id = u.usuario_id " +
                    "JOIN Pedidos p ON u.usuario_id = p.usuario_id " +
                    "JOIN Pedidos_Produtos pp ON p.pedido_id = pp.pedido_id " +
                    "JOIN Produtos pr ON pp.produto_id = pr.produto_id " +
                    "GROUP BY e.estorno_id, u.usuario_id, u.nome, p.pedido_id, p.total, p.data " +
                    "ORDER BY e.estorno_id " +
                    "LIMIT 10;";


    public List<Estorno> buscarEstornosCompletos() throws SQLCustomException {
        Map<Integer, Estorno> estornoMap = new HashMap<>();

        try (Connection conn = dbConnectionManager.getConnection();
             PreparedStatement statement = conn.prepareStatement(SQL_BUSCAR_ESTORNOS_COMPLETOS);
             ResultSet rs = statement.executeQuery()) {

            while (rs.next()) {
                Estorno estorno = extrairEstorno(rs, estornoMap);
                Pedido pedido = extrairPedido(rs, estorno);
                adicionarProdutos(rs, pedido);
            }

            return new ArrayList<>(estornoMap.values());

        } catch (SQLException e) {
            throw new SQLCustomException("Erro ao buscar estornos completos: " + e.getMessage());
        }
    }

    private Estorno extrairEstorno(ResultSet rs, Map<Integer, Estorno> estornoMap) throws SQLException {
        int estornoId = rs.getInt("estorno_id");
        int usuarioId = rs.getInt("usuario_id");
        String nomeUsuario = rs.getString("nome_usuario");

        return estornoMap.computeIfAbsent(estornoId, id -> {
            Usuario usuario = new Usuario(usuarioId, nomeUsuario);
            return new Estorno(estornoId, usuario);
        });
    }

    private Pedido extrairPedido(ResultSet rs, Estorno estorno) throws SQLException {
        int pedidoId = rs.getInt("pedido_id");
        BigDecimal totalPedido = rs.getBigDecimal("total_pedido");
        LocalDate dataCompra = rs.getDate("data_compra").toLocalDate();

        Usuario usuario = estorno.getUsuario();
        return usuario.getPedidos().stream()
                .filter(p -> p.getPedidoId() == pedidoId)
                .findFirst()
                .orElseGet(() -> {
                    Pedido novoPedido = new Pedido(pedidoId, totalPedido, dataCompra, new ArrayList<>());
                    usuario.adicionarPedido(novoPedido);
                    return novoPedido;
                });
    }

    private void adicionarProdutos(ResultSet rs, Pedido pedido) throws SQLException {
        String produtosIdStr = rs.getString("produtos_id");
        String valoresProdutosStr = rs.getString("valores_produtos");

        if (produtosIdStr != null && valoresProdutosStr != null) {
            List<Integer> produtosIdList = Arrays.stream(produtosIdStr.split(","))
                    .map(Integer::parseInt)
                    .collect(Collectors.toList());
            List<BigDecimal> valoresProdutosList = Arrays.stream(valoresProdutosStr.split(","))
                    .map(BigDecimal::new)
                    .collect(Collectors.toList());

            IntStream.range(0, produtosIdList.size()).forEach(i -> {
                int produtoId = produtosIdList.get(i);
                BigDecimal valorProduto = valoresProdutosList.get(i);
                Produto produto = new Produto(produtoId, valorProduto);
                pedido.adicionarProduto(produto);
            });
        }
    }
}
