package org.luizalabs.exception;
import java.sql.SQLException;

public class SQLCustomException extends SQLException {
    public SQLCustomException(String message) {
        super(message);
    }
}
