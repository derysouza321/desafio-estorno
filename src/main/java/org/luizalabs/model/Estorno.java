package org.luizalabs.model;

public class Estorno {
    private final int estornoId;
    private final Usuario usuario;

    public Estorno(int estornoId, Usuario usuario) {
        this.estornoId = estornoId;
        this.usuario = usuario;
    }

    public int getEstornoId() {
        return estornoId;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    @Override
    public String toString() {
        return "Estorno{" +
                "estornoId=" + estornoId +
                ", usuario=" + usuario +
                '}';
    }
}
