package org.luizalabs.model;

import java.math.BigDecimal;

public class Produto {
    private final int produtoId;
    private final BigDecimal valor;

    public Produto(int produtoId, BigDecimal valor) {
        this.produtoId = produtoId;
        this.valor = valor;
    }

    public int getProdutoId() {
        return produtoId;
    }

    public BigDecimal getValor() {
        return valor;
    }

    @Override
    public String toString() {
        return "Produto{" +
                "productId=" + produtoId +
                ", value=" + valor +
                '}';
    }
}
