package org.luizalabs.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public class Pedido  {
    private final int pedidoId;
    private final BigDecimal total;
    private final LocalDate date;
    private final List<Produto> produtos;

    public Pedido(int pedidoId, BigDecimal total, LocalDate date,List<Produto> produtos) {
        this.pedidoId = pedidoId;
        this.total = total;
        this.date = date;
        this.produtos = produtos;
    }

    public int getPedidoId() {
        return pedidoId;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public LocalDate getData() {
        return date;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void adicionarProduto(Produto produto) {
        produtos.add(produto);
    }

    @Override
    public String toString() {
        return "Pedido{" +
                "orderId=" + pedidoId +
                ", total=" + total +
                ", date=" + date +
                ", produtos=" + produtos +
                '}';
    }
}
