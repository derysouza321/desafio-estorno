package org.luizalabs.model;

import java.util.ArrayList;
import java.util.List;

public class Usuario {
    private final int usuarioId;
    private final String nome;
    private final List<Pedido> pedidos;

    public Usuario(int usuarioId, String nome) {
        this.usuarioId = usuarioId;
        this.nome = nome;
        this.pedidos = new ArrayList<>();
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public String getNome() {
        return nome;
    }

    public List<Pedido> getPedidos() {
        return pedidos;
    }

    public void adicionarPedido(Pedido pedido) {
        pedidos.add(pedido);
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "userId=" + usuarioId +
                ", name='" + nome + '\'' +
                ", pedidos=" + pedidos +
                '}';
    }
}
