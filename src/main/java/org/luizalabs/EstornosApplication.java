package org.luizalabs;
import org.luizalabs.connectiondb.DbConnectionManager;
import org.luizalabs.dto.EstornoDto;
import org.luizalabs.exception.SQLCustomException;
import org.luizalabs.service.ArquivoService;
import org.luizalabs.service.EstornosService;

import java.io.IOException;
import java.util.List;

import static org.luizalabs.service.util.Util.*;

public class EstornosApplication
{
    public static void main( String[] args ) throws SQLCustomException, IOException {
        ArquivoService arquivoService = new ArquivoService();
        EstornosService estornosService = new EstornosService();
        DbConnectionManager databaseService = new DbConnectionManager();
        boolean scriptProcessado = arquivoService.scriptJaProcessado(SCRIPT_FILE);

        if(!scriptProcessado){
            arquivoService.processarArquivos(PATH_DATA1,PATH_DATA2);
            databaseService.executarScriptInsert();
        }

        List<EstornoDto> estornos = estornosService.buscarEstornosCompletos();

        for (EstornoDto estorno : estornos) {
            System.out.println(estorno);
        }
    }
}
