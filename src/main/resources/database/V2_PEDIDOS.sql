CREATE TABLE IF NOT EXISTS Pedidos (
    pedido_id INT PRIMARY KEY,
    usuario_id INT,
    total DECIMAL(10, 2),
    data DATE,
    FOREIGN KEY (usuario_id) REFERENCES Usuarios (usuario_id)
);
