CREATE TABLE IF NOT EXISTS Produtos (
    produto_id INT PRIMARY KEY,
    valor DECIMAL(10, 2)
);
