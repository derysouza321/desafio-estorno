CREATE TABLE IF NOT EXISTS Pedidos_Produtos (
    pedido_id INT,
    produto_id INT,
    PRIMARY KEY (pedido_id, produto_id),
    FOREIGN KEY (pedido_id) REFERENCES Pedidos (pedido_id),
    FOREIGN KEY (produto_id) REFERENCES Produtos (produto_id)
);
