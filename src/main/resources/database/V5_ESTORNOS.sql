CREATE TABLE IF NOT EXISTS Estornos (
    estorno_id INT AUTO_INCREMENT PRIMARY KEY,
    usuario_id INT,
    FOREIGN KEY (usuario_id) REFERENCES Usuarios (usuario_id)
);
